//
// Created by mnowak on 21.04.18.
//

#include <zconf.h>
#include <ncurses.h>
#include "Item.h"

Item::Item(So_queue *queue) {
    position = 0;
    current_queue = 0;
    character = static_cast<char>(rand() % 26 + 'A');
    so_queue = queue;
    so_queue->path_in_mutex.lock();
    queue->queues[0][0] = this;
    so_queue->path_in_mutex.unlock();
    t = thread(&Item::run, this);
}

void Item::run() {
    while (!end_thread) {
//        so_queue->print_mutex.lock();
        switch (current_queue) {
            //  jesli jest na drodze do kolejki
            case 0: {
                so_queue->path_in_mutex.lock();
                //  jesli jest ostatni w drodze do kolejki
                if (position == so_queue->queues[current_queue].size() - 1) {
                    so_queue->queue_mutex.lock();
                    //  jesli moze wejsc do kolejki, wchodzi do kolejki
                    if (so_queue->queues[2][0] == nullptr) {
                        enter_queue();
                    }
                        // jesli nie moze wejsc do kolejki, przechodzi na poczatek okregu
                    else {
                        so_queue->circle_mutex.lock();
                        if (so_queue->queues[1][0] == nullptr)
                            enter_circle();
                        so_queue->circle_mutex.unlock();
                    }
                    so_queue->queue_mutex.unlock();
                }
                    // jesli nie jest ostatni w drodze do kolejki, idzie do przodu o 1
                else if (can_move_forward()) {
                    move_forward();
                }
                so_queue->path_in_mutex.unlock();
                break;
            }
                //  jesli krazy dookola
            case 1: {
                so_queue->circle_mutex.lock();
                //  jesli jest ostatni w okregu
                if (position == so_queue->queues[current_queue].size() - 1) {
                    so_queue->queue_mutex.lock();
                    //jesli moze wejsc do kolejki, wchodzi do kolejki
                    if (so_queue->queues[2][0] == nullptr) {
                        enter_queue();
                    }
                        //jesli nie moze wejsc do kolejki, przechodzi na poczatek okregu
                    else {
                        if (so_queue->queues[1][0] == nullptr)
                            enter_circle();
                    }
                    so_queue->queue_mutex.unlock();
                }
                    //  jesli nie jest ostatni w drodze do kolejki, idzie do przodu o 1
                else if (can_move_forward()) {
                    move_forward();
                }
                so_queue->circle_mutex.unlock();
                break;
            }
                //  jesli jest w kolejce
            case 2: {
                //  idzie do przodu
                unique_lock lk_queue(so_queue->ul_queue_mutex);
                if (!can_move_forward()) {
                    so_queue->cv_queue.wait(lk_queue, [this]() { return can_move_forward(); });
                }

                if (can_move_forward()) {
                    move_forward();
                }
                lk_queue.unlock();
                so_queue->cv_worker.notify_one();
                so_queue->cv_queue.notify_all();
                break;
            }
                //  jesli jest na drodze z kolejki
            case 3: {
                so_queue->path_out_mutex.lock();
                if (position == so_queue->queues[current_queue].size() - 1)
                    so_queue->queues[current_queue][position] = nullptr;
                if (can_move_forward()) {
                    move_forward();
                }
                so_queue->path_out_mutex.unlock();
                break;
            }
            default: {
                break;
            };
        }
        so_queue->print_mutex.lock();
        print();
        so_queue->print_mutex.unlock();
        usleep(so_queue->move_item_time);
    }
}

void Item::move_forward() {
    so_queue->queues[current_queue][position + 1] = so_queue->queues[current_queue][position];
    so_queue->queues[current_queue][position] = nullptr;

    position++;
}

void Item::print() {
    for (int i = 0; i < so_queue->queues[0].size(); i++) {
        if (so_queue->queues[0][i] != nullptr)
            mvaddch(4, i, so_queue->queues[0][i]->character);
        else
            mvaddch(4, i, '-');
    }
    for (int i = 0; i < so_queue->queues[1].size(); i++) {
        if (so_queue->queues[1][i] != nullptr)
            mvaddch(so_queue->circle_pos[i][0], so_queue->circle_pos[i][1], so_queue->queues[1][i]->character);
        else
            mvaddch(so_queue->circle_pos[i][0], so_queue->circle_pos[i][1], '*');
    }
    for (int i = 0; i < so_queue->queues[2].size(); i++) {
        if (so_queue->queues[2][i] != nullptr)
            mvaddch(4, i + 10, so_queue->queues[2][i]->character);
        else
            mvaddch(4, i + 10, '-');
    }
    for (int i = 0; i < so_queue->queues[3].size(); i++) {
        if (so_queue->queues[3][i] != nullptr)
            mvaddch(4, i + 16, so_queue->queues[3][i]->character);
        else
            mvaddch(4, i + 16, '-');
    }

    mvaddch(4, 15, ' ');
    refresh();
}

Item::~Item() {
    t.join();
}

void Item::enter_queue() {
    so_queue->queues[current_queue][position] = nullptr;
    position = 0;
    current_queue = 2;
    so_queue->queues[current_queue][position] = this;
}

void Item::enter_circle() {
    so_queue->queues[current_queue][position] = nullptr;
    position = 0;
    current_queue = 1;
    so_queue->queues[current_queue][position] = this;
}

bool Item::can_move_forward() {
    return position < so_queue->queues[current_queue].size() - 1 &&
           so_queue->queues[current_queue][position + 1] == nullptr;
}
