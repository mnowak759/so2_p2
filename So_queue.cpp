//
// Created by mnowak on 21.04.18.
//

#include <zconf.h>
#include <ncurses.h>
#include "So_queue.h"

So_queue::So_queue() {
    queues.resize(4);
    queues[0].resize(10, nullptr);
    queues[1].resize(30, nullptr);
    queues[2].resize(3, nullptr);
    queues[3].resize(10, nullptr);

    worker_thread = thread(&So_queue::worker, this);
    create_item_thread = thread(&So_queue::create_item, this);
}

void So_queue::worker() {
    Item *item;
    while (!stop) {
        unique_lock lk_worker(ul_worker_mutex);
        cv_worker.wait(lk_worker, [this]() { return queues[2][2] != nullptr; });
        unique_lock lk_queue(ul_queue_mutex);
        item = pop_item();

        lk_queue.unlock();
        ul_queue_mutex.unlock();
        cv_queue.notify_one();

        print_mutex.lock();
        mvaddch(4, 14, item->character);
        print_mutex.unlock();
        refresh();
        item->current_queue = -1;
        lk_worker.unlock();
        usleep(so_time);

        print_mutex.lock();
        mvaddch(4, 14, ' ');
        print_mutex.unlock();
        refresh();

        path_out_mutex.lock();
        push_item(item);
        path_out_mutex.unlock();
    }
}

Item *So_queue::pop_item() {
    Item *item = queues[2][queues[2].size() - 1];
    queues[2][queues[2].size() - 1] = nullptr;
    return item;
}

void So_queue::push_item(Item *item) {
    item->position = 0;
    item->current_queue = 3;
    queues[3][0] = item;
}

void So_queue::create_item() {
    while (true) {
        if (queues[0][0] == nullptr) {
            Item *item = new Item(this);
        }
        __useconds_t time = create_item_time + rand() % 1000000;
        usleep(time);
    }
}

