//
// Created by mnowak on 21.04.18.
//

#ifndef SO2_P2_ITEM_H
#define SO2_P2_ITEM_H

class So_queue;

#include <thread>
#include "So_queue.h"

using namespace std;

class Item {
public:
    int position;
    int current_queue;
    char character;

    Item(So_queue *queue);

    virtual ~Item();

    void run();

    void move_forward();

    void print();

    void enter_queue();

    void enter_circle();

    bool can_move_forward();

private:
    bool end_thread = false;

    So_queue *so_queue;
    thread t;
};


#endif //SO2_P2_ITEM_H
