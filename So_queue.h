//
// Created by mnowak on 21.04.18.
//

#ifndef SO2_P2_SO_QUEUE_H
#define SO2_P2_SO_QUEUE_H

class Item;

#include <vector>
#include <mutex>
#include <condition_variable>
#include "Item.h"

using namespace std;

class So_queue {
public:
    bool stop;
    mutex ul_worker_mutex;
    mutex ul_queue_mutex;
    mutex print_mutex;
    mutex path_in_mutex;
    mutex circle_mutex;
    mutex path_out_mutex;

    __useconds_t create_item_time = 1300000;
    __useconds_t so_time = 2999999;
    __useconds_t move_item_time = 199999;

    condition_variable cv_worker;
    condition_variable cv_queue;
    mutex queue_mutex;
    vector<vector<Item *>> queues;
    int circle_pos[30][2] = {
            {3, 10},
            {2, 10},
            {1, 10},
            {0, 10},
            {0, 11},
            {0, 12},
            {0, 13},
            {0, 14},
            {0, 15},
            {0, 16},
            {0, 17},
            {0, 18},
            {1, 18},
            {2, 18},
            {3, 18},
            {5, 18},
            {6, 18},
            {7, 18},
            {8, 18},
            {8, 17},
            {8, 16},
            {8, 15},
            {8, 14},
            {8, 13},
            {8, 12},
            {8, 11},
            {8, 10},
            {7, 10},
            {6, 10},
            {5, 10}
    };

    void worker();

    void create_item();

    So_queue();

private:
    thread worker_thread;
    thread create_item_thread;

    Item *pop_item();

    void push_item(Item *item);
};

#endif //SO2_P2_SO_QUEUE_H
